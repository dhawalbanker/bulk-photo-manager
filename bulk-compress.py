import fnmatch
import sys
import os
import PIL
from PIL import Image

#compress_image(img, i)
 
def compress_image(pic):
    #print u'{0}: compressing image: {1}'.format(index, pic.name)
    #input_file = StringIO(pic.get_contents_as_string())
	jpeg=Image.open(pic);
	print jpeg;
	ret=jpeg.save(pic+'_65.jpg',optimize=True,quality=65);
	print ret;
 
def start_compressing():
	matches = []
	for root, dirnames, filenames in os.walk('.'):
		for filename in filenames:
			if filename.endswith(('.jpg', '.jpeg', '.JPG')):
				matches.append(os.path.join(root, filename))

	for files in matches:
		jpeg=os.getcwd()+'/'+files;
		compress_image(jpeg);
		
 
if __name__ == "__main__":
    if len(sys.argv) > 1:
        command = sys.argv[1]
        if command == 'dry_run':
            print "Some arguments are passed";
        else:
            print 'Invalid command. For a dry run please run: python compress_s3_images.py dry_run'
    else:
        start_compressing();
    sys.exit()
